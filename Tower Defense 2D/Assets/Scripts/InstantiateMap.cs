using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class InstantiateMap : MonoBehaviour {

	public GameObject TilePrefab;
    public TextAsset level;
    public int rows;
    public int columns;
    // Use this for initialization
    void Start() {
        /*
        string fs = level.text;
        string[] fLines = Regex.Split(fs, "\n|\r|\r\n");

        for(int y = fLines.Length/2 * -1; y < fLines.Length/2; y++)
        {
            for (int x = fLines[0].Length / 2 * -1; x < fLines[0].Length / 2; x++)
            {
                Instantiate(TilePrefab, new Vector3(x, y), Quaternion.identity, this.transform);
            }
        }
        */
        for (int y = -1 * rows / 2; y < rows / 2; y++)
        {
            for(int x = -1*columns/2; x< columns/2; x++)
            {
                Instantiate(TilePrefab, new Vector3(x, y), Quaternion.identity, this.transform);
            }
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
