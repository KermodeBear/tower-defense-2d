﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSelect : MonoBehaviour {


    // Update is called once per frame
    private SpriteRenderer srender;
    public Color highlightColor = new Color(0.75f, 0.75f, 1f,1f);
    private void Start()
    {
        srender = GetComponent<SpriteRenderer>();
    }
    void Update () {
		
	}
    private void OnMouseEnter()
    {
        srender.color = highlightColor;
    }
    private void OnMouseExit()
    {
        srender.color = Color.white;
    }
    private void OnMouseDown()
    {
        Destroy(gameObject);
    }
}
