﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraMovement : MonoBehaviour
{
    public float lerpStuff = 20;
    public float zoomLerpStuff = 10;
    public float zoom = 5;
    public float dragScale = 0.003f;
    private Vector3 dragOrigin;

    private Vector3 clickOrigin = Vector3.zero;
    private Vector3 basePos = Vector3.zero;
    private Vector3 newPos;
    private Camera cam;

    void Start()
    {
        newPos = transform.position;
        cam = GetComponent<Camera>();
    }
    void LateUpdate()
    {
        zoom -= Input.mouseScrollDelta.y;
        if (zoom < 2)
            zoom = 2;
        if(Input.GetMouseButton(0))
        {
            if(clickOrigin == Vector3.zero)
            {
                clickOrigin = Input.mousePosition;
                basePos = transform.position;
            }
            dragOrigin = Input.mousePosition;
        }
        if (!Input.GetMouseButton(0))
        {
            clickOrigin = Vector3.zero;

        }
        else
        {
            newPos = new Vector3(basePos.x + ((clickOrigin.x - dragOrigin.x) * zoom * dragScale), basePos.y + ((clickOrigin.y - dragOrigin.y) * zoom*dragScale), -5);
        }
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime*lerpStuff);
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, zoom, Time.deltaTime * zoomLerpStuff);
    }

}

